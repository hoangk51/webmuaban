   @if ($records->lastPage() > 1)

  <ul class="pagination">
   <li>
    <a href="#" aria-label="Previous" onclick ="pagination(1)">
	 <span aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
    </a>
   </li>
    @for ($i = 1; $i <= $records->lastPage(); $i++)
   <li class=" {{ ($records->currentPage() == $i) ? ' active' : '' }}"><a href="#" onclick ="pagination({{$i}})">{{$i}}</a></li>
    @endfor
   <li>
	 <a href="#" aria-label="Next"  onclick ="pagination({{$records->lastPage()}})" >
	  <span aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
	 </a>
   </li>
  </ul>
  @endif


  <script type="text/javascript">
    function pagination(page) {
        $('#page').val(page);
        var form = $('#form');
        data = form.serialize();
        data = data + '&page='+page;
        doSearch(data);
    }
</script>