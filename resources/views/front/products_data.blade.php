<div class="row "> 
      	 	<form id="form" action="{{route('product.search')}}">
		 	<div class="col-md-3 form-group">
		 		<input class="form-control" type="text" name="name" placeholder="tìm kiếm theo tên sản phẩm" onchange="filter()" value="{{request()->input('name')}}" >
		 	</div>

		 	<div class="col-md-3 form-group">
		 		<input class="form-control" type="text" name="code" placeholder="tìm kiếm theo mã" onchange="filter()" value="{{request()->input('code')}}">
		 	</div>

		 	<div class="col-md-3 form-group">
		 		<button class="kafe-btn kafe-btn-mint-small" type="button" onclick="filter()" > Tìm kiếm  </button>
		 	</div>
		 </form>
		</div> 	

	   <div class="work">
	    @foreach($products as $product)
		 <div class="job">	
		  
		  <div class="row top-sec">
		   <div class="col-lg-12">
			<div class="col-lg-12 col-xs-12"> 
			 <h4><a href="{{route('product.detail',$product->id)}}">{!! $product->name !!}</a></h4>
			<!--  <h5>Web Design <small>- Web development</small></h5>
			 <h6>Hourly $$ (intermediate) 40+ hours per week</h6>
			 <p><small>Posted 14 Hours ago</small></p> -->
			</div><!-- /.col-lg-10 -->
			
		   </div><!-- /.col-lg-12 -->
		  </div><!-- /.row -->
		  
		  <div class="row mid-sec">			 
		   <div class="col-lg-12">			 
		   <div class="col-lg-12">
			<hr class="small-hr">
			<p>{!! $product->description !!}</p>
			<span class="label label-success">{{$product->category->name}}</span>
			
		   </div><!-- /.col-lg-12 -->
		   </div><!-- /.col-lg-12 -->
		  </div><!-- /.row -->
		  
		  <div class="row bottom-sec">
		   <div class="col-lg-12">
			
			<div class="col-lg-12">
			 <hr class="small-hr">
			</div> 
			
			<div class="col-lg-6">
			 <div class="pull-left">
			  <a href="profile.html">
			   <img class="img-responsive" src="{{$product->getLargeThumb()}}" alt="Image">
			  </a>
			 </div><!-- /.col-lg-2 -->
			 <!-- <h5> Vanessa Wells </h5>
			 <p><i class="fa fa-map-marker"></i> Kenya</p>
				<p class="p-star"> 
				 <i class="fa fa-star rating-star"></i>
				 <i class="fa fa-star rating-star"></i>
				 <i class="fa fa-star rating-star"></i>
				 <i class="fa fa-star rating-star"></i>
				 <i class="fa fa-star-o rating-star"></i>
				</p> -->
			</div>
			<div class="col-lg-3">
			 <div class="pull-right">
			  <h4> {{ $product->sellers->count() }} </h4>
			  <p> Người bán</p>
			 </div>
			</div>
			<div class="col-lg-3">
			 <div class="pull-right">
			  <h4> {{ $product->customers->count() }} </h4>
			  <p> Người theo dõi</p>
			 </div>
			</div>
		   </div><!-- /.col-lg-12 -->
		  </div><!-- /.row -->
		 
		 </div><!-- /.job -->	   
		 @endforeach
        <div class="paginationCommon blogPagination text-center">
		 <nav aria-label="Page navigation">
		  @include('front.pagination',['records'=>$products])
		
		 </nav>
		</div>
	   
       </div><!-- ./work -->		
	  