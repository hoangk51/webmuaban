@extends('front.layout.app')
@section('content')
@php 
$sellers = $product->sellers;
$userId = Auth()->user()->id; 
@endphp

<section class="jobpost">	
      <div class="container">
	   <div class="row"> 
	   
        <div class="card-box-profile">
		  <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
		   
		  <div class="row bottom-sec">
		   <div class="col-lg-12">
		   <h3>{{$product->name}}</h3>
			<img src="{{$product->getLargeThumb()}}">
			<p>{{$product->description}}</p>

			<div class="col-lg-12">
			 <hr class="small-hr">
			</div> 
			
			<!-- <div class="col-lg-2">
			 <h5> Posted </h5>
			 <p>4 Hours Ago</p>
			</div>
			<div class="col-lg-2">
			 <h5> Location </h5>
			 <p><i class="fa fa-map-marker"></i> Kenya</p>
			</div>
			<div class="col-lg-2">
			 <h5> Budget </h5>
			 <p>$220</p>
			</div>
			<div class="col-lg-2">
			 <h5> Duration </h5>
			 <p>Not Sure</p>
			</div> -->
			@if(Auth()->check())
			<div class="col-lg-3" >
				 @if(!in_array($userId,$sellerArr))
                <a href="{{route('client.product.sell',$product->id)}}" class="kafe-btn kafe-btn-mint-small"><i class="fa fa-align-left"></i> Đăng bán</a>
                @else
                <span class="badge badge-success" >
                Đang bán sp này
                 </span>
                @endif
			</div>
			<div class="col-lg-3 pull-right" id="follow">
				@if(!in_array($userId,$customerArr))
                   <a class="kafe-btn kafe-btn-mint-small"  onclick="follow()"><i class="fa fa-align-left"></i> Theo dõi mã</a>
                  @else
                    <span class="badge label-danger" >
                         Đã theo dõi
                    </span>
                @endif
			</div>
			@endif
		   </div><!-- /.col-lg-12 -->
		  </div><!-- /.row -->
		 	   
		  </div><!-- .col-lg-12 -->
		</div><!-- .card-box-profile --> 
		
       </div><!-- .row -->	  
      </div><!-- .container -->	
     </section>

     <section class="profile-details">
	  <div class="container">
	   <div class="row">
	   
	    <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
		 <div class="card-box-profile-details">

		   <div class="description-profile">
		   
		    <ul class="tr-list resume-info">			
		   
             <li>
			  <div class="icon">
			   <p class="tr-title"><i class="fa fa-black-tie" aria-hidden="true"></i> Job Description</p>
			  </div>  
			  <div class="media-body">
			   <p>{!!$product->description!!}</p>
			  </div>
		     </li><!-- /.career-objective-->			
		   	
			 
            </ul><!-- /.ul -->			
			 
		   </div><!-- /.description-profile -->	
				
		</div><!-- .card-box-profile-details -->	


	   <div class="work">
			
			<div class="col-lg-12">
			  <div class="icon">
			   <p class="tr-title"><i class="fa fa-users" aria-hidden="true"></i> Danh sách đăng bán  ({{count($sellers)}})</p>
			  </div> 
			</div> 
	   
	   
	   	@foreach($sellers as $seller)
		 <div class="job">	
		  
		  <div class="row bottom-sec">
		   <div class="col-lg-12">
			
			<div class="col-lg-12">
			 <hr class="small-hr">
			</div> 
			
			<div class="col-lg-12">
			 <div class="pull-left">
			  <a href="{{route('profile',$seller->user->id)}}">
			   <img class="img-responsive" src="{{$seller->user->getSmallThumb()}}" alt="Image">
			  </a>
			 </div><!-- /.col-lg-2 -->
			 <h5><a href="{{route('profile',$seller->user->id)}}"> {{$seller->user->name}} {{ $seller->user->rating }}</a> </h5>
			 <p><i class="fa fa-map-marker"></i> {{$seller->city_id}}</p>
				<p class="p-star"> 
				@for( $i= 0 ; $i< $seller->user->rating  ; $i++ )
				 <i class="fa fa-star rating-star"></i>
				 @endfor
				</p>
			</div>
			
		   </div><!-- /.col-lg-12 -->
		  </div><!-- /.row -->
		  
		  <div class="row mid-sec">			 
		   <div class="col-lg-12">			 
		   <div class="col-lg-12">
			<hr class="small-hr">
			<p>{!!$seller->user->description!!}</p>
			<span class="label label-success">Tải báo giá</span>
			<span class="label label-success">Gửi email</span>
			<span class="label label-success">Liên hệ ncc</span>
		
		   </div><!-- /.col-lg-12 -->
		   </div><!-- /.col-lg-12 -->
		  </div><!-- /.row -->
		 
		 </div><!-- /.job -->
	   
		@endforeach

       </div><!--/ .work --> 		 
		
		
       </div><!-- .col-lg-9 -->
	   
	    <div class="col-lg-3 col-md-3 col-sm-8 col-xs-12">
		
		 <div class="stats">
		   <div class="row">
		     <h5>Requirements</h5>
		    <div class="col-sm-6">
			 <h6>Số người bán</h6>
			 <p class="bottom">{{count($product->sellers)}} </p>
			</div>
		    <div class="col-sm-6">
			 <h6>Số lượng quan tâm</h6>
			 <p class="bottom">{{count($product->customers)}} </p>
			</div>
		   </div>
		   <div class="row">
		    <div class="col-sm-6">
			 <h6>Số lượng bán</h6>
			 <p class="bottom">Hours Worked</p>
			</div>
		    <div class="col-sm-6">
			 <h6>Số lượt xem</h6>
			 <p class="bottom">{{$product->view}}</p>
			</div>
		   </div>
		</div><!-- .stats -->	
		
		 <div class="stats">
		   <div class="row">
		     <h5>Số lượng bán</h5>
		    <div class="col-sm-4">
			 <h6>3</h6>
			 <p class="bottom">Số lượng bán</p>
			</div>
		    <div class="col-sm-4">
			 <h6>{{$product->view}}</h6>
			 <p class="bottom">Số lượt xem</p>
			</div>
		    <div class="col-sm-4">
			 <h6>0</h6>
			 <p class="bottom">Hired</p>
			</div>
			 <p class="bottom"> Last viewed by client: <b> 3 days ago </b></p>
		   </div>
		</div><!-- .stats -->	
		
         <div class="card-box text-center">
		  <div class="clearfix"></div>
		  <div class="member-card">
		  
		   <div class="thumb-xl member-thumb m-b-10 center-block">
		    <img src="assets/img/users/4.jpg" class="img-circle img-thumbnail" alt="profile-image">
			<i class="fa fa-star member-star text-success" title="verified user"></i>
		   </div>
			<h5>Julie L. Arsenault</h5>
		   
		   <div class="row">
		    <div class="col-sm-6">
			 <h4 class="top">1</h4>
			 <p class="bottom">Jobs Posted</p>
			</div>
		    <div class="col-sm-6">
			 <h4 class="top">$0.00</h4>
			 <p class="bottom">Spent</p>
			</div>
		   </div>
		   
		  </div>
		 </div>
			
		
		
       </div><!-- .col-lg-3 -->
	   
      </div><!-- .row -->	  
	 </div><!-- .container -->
    </section>


@endsection

@section('js')

    <script type="text/javascript">
    	function sell(){

    		var url='{{route('client.product.sell',$product->id)}}';
    		$.ajax({
		        url: url,
		        type: 'POST',
		        dataType: "JSON",
		        data: formData,
		        cache:false,
		        contentType: false,
		        processData: false,
		        success: function (response) {
		            if (response.status === "success") {
		                toastr.success('Cap nhat thanh cong!');
		                $("#myDynamicModal").modal('hide');
		                //$('#table').html( response.html );
		                filter();
		               // showHideTable();
		            } else {
		               toastr.error('error!');
		            }

		        },
		        error: function (xhr) {
		            const errors =xhr.responseJSON.errors;
		             $.each( errors, function( key, value ) {
		                    const dom = document.getElementById(key);
		                     dom.nextSibling.remove();
		                    dom.insertAdjacentHTML('afterend','<div class="text-danger">'+value[0]+'</div>');
		                    dom.classList.add('is-invalid');
		                    dom.parentNode.parentNode.classList.add("validated");
		                    dom.parentNode.classList.add("has-error");
		                    dom.parentNode.classList.add("has-feedback");
		             });
		        }
		    });
    	}

    	function follow(){

    		var token = $('#token').val();
    		//alert(token);
    		var url ='{{route('client.product.follow',$product->id)}}';
    		$.ajax({
         type:'POST',
		  url: url,
         headers: {'X-CSRF-TOKEN': token},
         success:function(response){
             if (response.status === "success") {
             	$('#follow').html('<span class="badge label-danger">Đã theo dõi</span>');
                toastr.success(response.message);
            } else {
               toastr.error('error!');
            }
         }
      });
    		
    		
    	}
    </script>
@endsection
