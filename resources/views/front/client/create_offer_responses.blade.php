@extends('front.layout.dashboard')

@section('content')
<div class="col-sm-8 col-md-9">		

		  <div class="job-box">
		   <div class="job-header">
		    <h4>Tạo đơn hỏi hàng</h4>
		   </div>
		 {!! Form::open(['route' => 'client.response.offer.store','id'=>'addform']) !!}
		  {!! Form::hidden('request_id',old('request_id',$offer->id)) !!}
              <div class="form-group">	
			    <label>Giá bán</label>
                <input type="text" name="price" class="form-control" placeholder="Giá" value="">
              </div>   

			  <div class="form-group">	
			   <label>Mô tả</label>
			   <textarea name="description" class="form-control" rows="5" placeholder="Provide a more detailed description of your job to get better proposals."></textarea>
			  </div> 
			  
              <button class="kafe-btn kafe-btn-mint-small full-width">Submit</button>
           {!! Form::close() !!}
          </div><!-- /.job-box -->		
		
	    </div>

@endsection
