@extends('front.layout.dashboard')

@section('content')
  <div class="col-sm-8 col-md-9">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Đơn hàng </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                 <div class="table-responsive">
                  <table id="" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Người bán</th>
                        <th>Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Giá kì vọng</th>
                        <th>Thời gian tạo</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($orders as $order)
                      <tr>
                        <td>{{$order->seller->user->name}}</td>
                        <td>{{$order->product->name}}</td>
                        <td>{{$order->quantity}}</td>
                        <td>{{$order->price}}</td>
                        <td>{{$order->created_at}}</td>
                        <td>
                           <a onclick="responseList()" href="#" class="btn btn-success btn-xs" data-toggle="tooltip" title="Xem đơn hàng"><span class="fa fa-eye"></span></a>
                          
                           </tr>
                      @endforeach
                    </tbody>
                  
                  </table>
                 </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->        
      
    
    
      </div><!-- /.col-md-9 --> 
         
   
@endsection


@section('js')
<script type="text/javascript">

</script>
@endsection