@extends('front.layout.dashboard')

@section('content')
@php 
$buyer = \Auth()->user();
$orderStatusArr = \App\Constant\OrderStatus::$types;
@endphp
<div class="col-sm-8 col-md-9">		

		  <div class="job-box">
		   <div class="job-header">
		    <h4>Tạo đơn hỏi hàng</h4>
		   </div>

		   <div class="col-lg-6">

              <div class="form-group">	
			    <label>Thông tin chào bán</label>
                <span>  {{ $response->description }}</span>
              </div>   
            </div> 


            <div class="col-lg-6">
              <div class="form-group">	
			    <label>Giá bán</label>
                <span>  {{ $response->price }}</span>
              </div>   
            </div> 

              <div class="col-lg-6">
              <div class="form-group">	
			    <label>Số lượng cung cấp</label>
                <span>  {{ $response->quantity }}</span>
              </div>   
            </div> 

            <div class="col-lg-6">
              <div class="form-group">	
			    <label>Địa điểm</label>
                <span>  {{ $response->price }}</span>
              </div>   
            </div> 





		 {!! Form::open(['route' => 'client.buy.order.store','id'=>'addform']) !!}
		  {!! Form::hidden('response_id',old('response_id',$response->id)) !!}
		 {!! Form::hidden('seller_id',old('seller_id',$response->seller_id)) !!}
		 {!! Form::hidden('customer_id',old('customer_id',\Auth()->user()->customer_id)) !!}
        {!! Form::hidden('product_id',old('product_id',$response->product_id)) !!}

		   	<div class="col-lg-6">

              <div class="form-group">	
			    <label>Tên người mua</label>
              {!! Form::text('buyer_name', (old('buyer_name',$buyer->name)), ['class' => 'form-control']) !!}
              </div>   
            </div> 

            <div class="col-lg-6">

              <div class="form-group">	
			    <label>Email</label>
              {!! Form::text('buyer_email', (old('buyer_email',$buyer->email)), ['class' => 'form-control']) !!}

              </div>   
            </div>

            <div class="col-lg-6">

              <div class="form-group">	
			    <label>Số điện thoại</label>
              {!! Form::text('buyer_phone', (old('buyer_phone',$buyer->phone)), ['class' => 'form-control']) !!}
              </div>   
            </div> 

             <div class="col-lg-6">

              <div class="form-group">	
			    <label>Số lượng</label>
                <input type="text" name="quantity" class="form-control" placeholder="Số lượng" value="">
              </div>   
            </div> 


             <div class="col-lg-6">

              <div class="form-group">  
          <label>Giá</label>
              {!! Form::text('price',old('price',$response->price),['class'=>'form-control'] ) !!}

              </div>   
            </div> 

            <div class="col-lg-6">

              <div class="form-group">  
                <label>Mã đơn hàng</label>
              {!! Form::text('code',null, ['class' => 'form-control']) !!}
              </div>   
            </div> 


             <div class="col-lg-6">

              <div class="form-group">  
                <label>Mã số thuế</label>
                  {!! Form::text('buyer_mst',null, ['class' => 'form-control']) !!}
              </div>   
            </div> 


             <div class="col-lg-6 job-sec">
              <div class="form-group">  
                <label>Trạng thái đơn hàng</label>
            {!! Form::select('status',$orderStatusArr,old('status'),['class'=>'form-control']) !!}

              </div> 
            </div>

            <div class="col-lg-6">

              <div class="form-group">  
                <label>Ngày tạo</label>
                <input type="text" name="create_date" class="form-control datepicker" placeholder="ngày tạo" value="">
              </div>   
            </div> 

            <div class="col-lg-6">

              <div class="form-group">  
                <label>Ngày thanh toán </label>
                <input type="text" name="payment_date" class="form-control datepicker" placeholder="ngày thanh toán" value="">
              </div>   
            </div> 

            <div class="col-lg-6">

              <div class="form-group">  
                <label>Ngày giao hàng</label>
                <input type="text" name="shipping_date" class="form-control datepicker" placeholder="ngày giao hàng" value="">
              </div>   
            </div> 

			  <div class="form-group">	
			   <label>Description</label>
			   <textarea name="description" class="form-control" rows="5" placeholder=""></textarea>
			  </div> 
			  
        <div class="form-group">  
         <label>Địa điểm giao hàng</label>
         <textarea name="description" class="form-control" rows="5" placeholder=""></textarea>
        </div> 

              <button class="kafe-btn kafe-btn-mint-small full-width">Submit</button>
           {!! Form::close() !!}
          </div><!-- /.job-box -->		
		
	    </div>



@endsection


@section('css')
    <link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@endsection

@section('js')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js
"></script>

<script type="text/javascript">
  $(document).ready(function() {
        $(".datepicker").datetimepicker({
            format: 'YYYY-MM-DD hh:mm:ss',
            icons:{
                time: 'glyphicon glyphicon-time',
                date: 'glyphicon glyphicon-calendar',
                previous: 'glyphicon glyphicon-chevron-left',
                next: 'glyphicon glyphicon-chevron-right',
                today: 'glyphicon glyphicon-screenshot',
                up: 'glyphicon glyphicon-chevron-up',
                down: 'glyphicon glyphicon-chevron-down',
                clear: 'glyphicon glyphicon-trash',
                close: 'glyphicon glyphicon-remove'
            }
        });
});


</script>
@endsection

