@extends('front.layout.dashboard')

@section('content')
<div class="col-sm-8 col-md-9">		

		  <div class="job-box">
		   <div class="job-header">
		    <h4>Tạo đơn hỏi hàng</h4>
		   </div>
		 {!! Form::open(['route' => 'client.offer.store','id'=>'addform']) !!}
		  {!! Form::hidden('product_id',old('product_id',request()->input('product_id'))) !!}
		   	<div class="col-lg-6">

              <div class="form-group">	
			    <label>Số lượng</label>
                <input type="number" name="quantity" class="form-control" placeholder="Số lượng" value="">
              </div>   
            </div> 

            <div class="col-lg-6">

              <div class="form-group">	
			    <label>Giá kì vọng</label>
                <input type="text" name="price" class="form-control" placeholder="Giá" value="">
              </div>   
            </div> 

			  <div class="form-group">	
			   <label>Description</label>
			   <textarea name="description" class="form-control" rows="5" placeholder="Provide a more detailed description of your job to get better proposals."></textarea>
			  </div> 
			  
              <button class="kafe-btn kafe-btn-mint-small full-width">Submit</button>
           {!! Form::close() !!}
          </div><!-- /.job-box -->		
		
	    </div>

@endsection
