@extends('front.layout.dashboard')

@section('content')
<div class="col-sm-8 col-md-9">		

		  <div class="job-box">
		   <div class="job-header">
		    <h4>Tạo đơn hỏi hàng</h4>
		   </div>
		 {!! Form::open(['route' => 'client.product.store','id'=>'addform']) !!}
		   	 <div class="col-lg-6">

              <div class="form-group">	
			    <label>Tên</label>
                <input type="text" name="name" class="form-control" placeholder="Tên sản phẩm" value="">
              </div>   
            </div> 

            <div class="col-lg-6">

              <div class="form-group">	
			    <label>Mã</label>
                <input type="text" name="code" class="form-control" placeholder="mã sản phẩm" value="">
              </div>   
            </div> 

			  <div class="form-group">	
			   <label>Description</label>
			   <textarea name="description" class="form-control" rows="5" placeholder=""></textarea>
			  </div> 
			 
			 <div class="form-group col-sm-6">
			    {!! Form::label('category_id', 'Category:') !!}
			    {!! Form::select('category_id', $categories,null, ['class' => 'form-control']) !!}
			</div>

			<input type="file" name="thumbnail">

              <button class="kafe-btn kafe-btn-mint-small full-width">Submit</button>
           {!! Form::close() !!}
          </div><!-- /.job-box -->		
		
	    </div>

@endsection
