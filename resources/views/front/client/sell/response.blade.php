@extends('front.layout.dashboard')

@section('content')
  <div class="col-sm-8 col-md-9">
      
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Danh sách đơn chào hàng </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                 <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Sản phẩm</th>
                        <th>Giá kì vọng</th>
                        <th>Số lượng hỏi mua</th>
                        <th>Thời gian hỏi hàng</th>
                        <th>Người mua</th>
                        <th>Giá bán</th>
                         <th>Số lượng bán</th>
                         <th>Thời gian trả lời</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($responses as $response)
                      <tr>
                        <td>{{$response->product_name}}</td>
                        <td>{{$response->customer_price}}</td>
                        <td>{{$response->quantity}}</td>
                        <td>{{$response->request_time}}</td>
                         <td>{{$response->buyer_name}}</td>
                        <td>{{$response->price}}</td>
                        <td>{{$response->sell_quantity}}</td>
                        <td>{{$response->responses_time}}</td>
                        <td>
                            <!-- <a href="#" class="btn btn-success btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-edit"></span></a></td>
                             -->                      
                           </tr>
                      @endforeach
                    </tbody>
                   
                  </table>
                 </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->  
   </div>                  
@endsection
