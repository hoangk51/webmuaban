@extends('front.layout.dashboard')

@section('content')
  <div class="col-sm-8 col-md-9">
      
      <div class="pro-nav text-center">
           <ul class="nav pro-nav-tabs nav-tabs-dashed">
            <li><a href="workroom.html">Overview & Discussions</a></li>
            <li><a href="milestone.html">Milestones</a></li>
            <li class="active"><a href="task.html">Tasks</a></li>
            <li><a href="timesheet.html">Timesheets</a></li>
            <li><a href="file.html">Files</a></li>
            <li><a href="link.html">Links</a></li>
            <li><a href="bug.html">Bugs</a></li>
            <li><a href="payment.html">Payments</a></li>
            <li><a href="rate.html">Rate the Freelancer</a></li>
              
           </ul>  
          </div><!-- /.pro-nav -->

      <div class="button-box">
       <a href="#addm" class="kafe-btn kafe-btn-mint-small" data-toggle="modal">Add Task</a>
          </div><!-- /.prop-info -->      

      
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Sản phẩm đang theo dõi </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                 <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Milestone</th>
                        <th>Date to start</th>
                        <th>Date to end</th>
                        <th>Progress</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($products as $product)
                      <tr>
                        <td>{{$product->name}}</td>
                        <td>Research</td>
                        <td>{{$product->created_at}}</td>
                        <td>31th July 2018</td>
                        <td>
                        <div class="progress-xxs not-rounded mb-0 inline-block progress" style="width: 100%; margin-right: 5px">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;" data-toggle="tooltip" title="30%"></div>
                        </div>
                        </td>
                        <td>
                         <a href="{{route('client.offer.create',['product_id'=>$product->id])}}" class="btn btn-success btn-xs" data-toggle="tooltip" title="View"><span class="fa fa-eye"></span></a>
                         <a href="#" class="btn btn-success btn-xs" data-toggle="tooltip" title="Edit"><span class="fa fa-edit"></span></a></td>
                      </tr>
                      @endforeach
                    </tbody>
                   <!--  <tfoot>
                      <tr>
                        <th>Name</th>
                        <th>Milestone</th>
                        <th>Date to start</th>
                        <th>Date to end</th>
                        <th>Progress</th>
                        <th>Action</th>
                      </tr>
                    </tfoot> -->
                  </table>
                 </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->        
      
    
    
      </div><!-- /.col-md-9 --> 
         
@endsection
