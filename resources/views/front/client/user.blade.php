@extends('front.layout.dashboard')

@section('content')
<div class="col-sm-8 col-md-9">		

		  <div class="job-box">
		   <div class="job-header">
		    <h4>Edit your Profile</h4>
		   </div>
		 {!! Form::model($user, ['route' => ['client.user.update'],'id'=>'addform','files'=>true]) !!}
   	
              <div class="form-group">	
			    <label>Name</label>
                 {!! Form::text('name', null, ['class' => 'form-control']) !!}

              </div>   
		   	
		   	 <div class="form-group">	
			    <label>Avatar</label>
                <input type="file" name="thumbnail" class="form-control" placeholder="Name" >
              </div>  

              <div class="form-group">	
			    <label>Phone</label>
			     {!! Form::text('phone', null, ['class' => 'form-control']) !!}
              </div>   
		   
              <div class="form-group">	
			    <label>Location</label>
			     {!! Form::select('city_id', $cities,null, ['class' => 'form-control']) !!}
              </div>  
		   
              <div class="form-group">	
			    <label>About</label>
                <textarea name="description" class="form-control" rows="5">{{$user->description}}</textarea> 

              </div>
		   
          	  <div class="form-group">	
			    <label>Address</label>
			     {!! Form::text('address', null, ['class' => 'form-control']) !!}
              </div>   
			 <div class="form-group">	
			    <label>Mã số thuế</label>
			     {!! Form::text('mst', null, ['class' => 'form-control']) !!}
              </div>   
			  
              <button class="kafe-btn kafe-btn-mint-small full-width">Submit</button>
           {!! Form::close() !!}
          </div><!-- /.job-box -->		
		
	    </div>
         
@endsection
