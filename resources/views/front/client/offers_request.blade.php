@extends('front.layout.dashboard')

@section('content')
  <div class="col-sm-8 col-md-9">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Đơn hỏi hàng </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                 <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Người mua</th>
                        <th>Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Giá kì vọng</th>
                        <th>Thời gian tạo</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($offers as $offer)
                      <tr>
                        <td>{{$offer->customer->user->name}}</td>
                        <td>{{$offer->product->name}}</td>
                        <td>{{$offer->quantity}}</td>
                        <td>{{$offer->price}}</td>
                        <td>{{$offer->created_at}}</td>
                        <td>
                           <a onclick="responseList('{{route('client.buy.response.index',$offer->id)}}')" href="#" class="btn btn-success btn-xs" data-toggle="tooltip" title="Gửi chào hàng"><span class="fa fa-eye"></span></a>
                          
                           </tr>
                      @endforeach
                    </tbody>
                   <!--  <tfoot>
                      <tr>
                        <th>Name</th>
                        <th>Milestone</th>
                        <th>Date to start</th>
                        <th>Date to end</th>
                        <th>Progress</th>
                        <th>Action</th>
                      </tr>
                    </tfoot> -->
                  </table>
                 </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->        
      
    
    
      </div><!-- /.col-md-9 --> 
         

 <!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle">Danh sách đơn chào hàng</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modalContent">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>        
@endsection


@section('js')
<script type="text/javascript">
function responseList(url){
  $.ajax({
            url: url ,
            type: 'GET',
            data: {
               //"code": code,
            },
            success: function (html) {
                $("#modalContent").html(html);
                $("#modal").modal('show');

            },
            error: function (xhr) {
               
            }
        });
}
</script>
@endsection