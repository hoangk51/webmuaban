<!DOCTYPE html>
<html lang="en">
  <head>

	    <!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>The Kafe - Ultimate Freelance Marketplace Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Add your business website description here">
        <meta name="keywords" content="Add your, business, website, keywords, here">
        <meta name="author" content="Add your business, website, author here">
		
		<!-- ==============================================
		Favicons
		=============================================== --> 
		<link rel="icon" href="images/logo.jpg">
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
		
	    <!-- ==============================================
		CSS
		=============================================== -->
        <!-- Style-->
        <link type="text/css" href="{{asset('front/css/style.css')}}" rel="stylesheet">
		<link href="{{asset('front/css/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
				
		<link type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<script src="{{asset('front/js/modernizr-custom.js')}}"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->	
		
  </head>

  <body>

     <!-- ==============================================
     Navigation Section
     =============================================== -->  
 	@include('front.partials.header')

     <!-- ==============================================
	 Dashboard Section
	 =============================================== -->
     <section class="dashboard section-padding">
	  <div class="container">
	   <div class="row">
	   
		 	@include('front.partials.left_menu')
		 	 @yield('content')
	    
	        </div><!-- /.row -->
	    </div><!-- /.container -->
	</section>	 
	 
	 
	 <!-- ==============================================
	 Footer Section
	 =============================================== -->
	 	@include('front.partials.footer')


	 <!-- ==============================================
	 Scripts
	 =============================================== -->
     <script src="{{asset('front/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('front/js/bootstrap.min.js')}}"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="{{asset('front/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('front/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
      $(function () {
        $("#example1").dataTable();
      });
    </script>
	<script src="{{asset('front/js/kafe.js')}}"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

	@yield('js')

  </body>
</html>