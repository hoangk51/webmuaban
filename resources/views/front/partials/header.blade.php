@php
$user= \Auth()->user();
@endphp
    <header class="tr-header">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
	    <div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		 </button>
		 <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('front/images/logo.jpg')}} " alt="Image">Web mua bán</a>
		</div><!-- /.navbar-header -->
		<div class="navbar-left">
		 <div class="collapse navbar-collapse" id="navbar-collapse">
		  <ul class="nav navbar-nav">
		   <li><a href="{{route('home')}}">Trang chủ</a></li>
		   <li><a href="{{route('product.list')}}">Sản phẩm</a></li>
		   <li><a href="#">Bảng giá</a></li>
		   <li><a href="#">Hướng dẫn</a></li>
		  </ul>
		 </div>
		</div><!-- /.navbar-left -->
		<div class="navbar-right">                          
		 <ul class="nav navbar-nav">
		  <li><i class="fa fa-user"></i></li>
		  	@if(\Auth()->check())
		 <li class="dropdown mega-avatar active">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
		  
		   <span class="avatar w-32"><img src="{{$user->getLargeThumb()}}" class="img-resonsive img-circle" width="25" height="25" alt="..."></span>
		   <!-- hidden-xs hides the username on small devices so only the image appears. -->
		   <span>
			{{$user->name}}
		   </span>
		  
		  </a>
		  <div class="dropdown-menu w dropdown-menu-scale pull-right">
		   <a class="dropdown-item" href="{{route('client.dashboard')}}"><span>Dashboard</span></a> 
		   <a class="dropdown-item" href="{{route('client.user.edit')}}"><span>Profile</span></a> 
		   <a class="dropdown-item" href="editprofile.html"><span>Settings</span></a> 

		   <a class="dropdown-item" href="#"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
		   
		   Sign out
			</a>
			 <form id="logout-form" action="{{ url('/dang-xuat') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
		  </div>
		 </li><!-- /navbar-item -->	
		  @else
		  <li><a href="{{route('client.login')}}">Sign In </a></li>
 		 <li><a href="{{route('client.register')}}"> Register </a></li>
		  @endif
		 </ul><!-- /.sign-in -->   
		 <a href="{{route('client.product.create')}}" class="kafe-btn kafe-btn-mint-small">Post a Job</a>
		</div><!-- /.nav-right -->
       </div><!-- /.container -->
      </nav><!-- /.navbar -->
     </header><!-- Page Header --> 
	