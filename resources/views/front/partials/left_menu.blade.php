
	    <div class="col-sm-4 col-md-3">
		
		  <ul class="sidebar-menu" data-widget="tree">
			<li class="active">
			  <a href="dashboard.html">
				<i class="fa fa-life-ring"></i> <span>Dashboard</span>
			  </a>
			</li>
			<li>
			  <a href="contract.html">
				<i class="fa fa-align-left"></i> <span>Contracts</span>
			  </a>
			</li>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-files-o"></i> <span>Mua</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
			  	<li><a href="{{route('client.buy.follow.products')}}">
					<i class="fa fa-circle-o"></i> <span>Đang theo dõi</span>
				  </a>
				 </li> 
				<li><a href="{{route('client.buy.offer.index')}}"><i class="fa fa-circle-o"></i>Hỏi hàng</a></li>
				<li><a href="{{route('client.buy.order.list')}} "><i class="fa fa-circle-o"></i> Đơn hàng</a></li>
			  </ul>
			</li>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-files-o"></i> <span>Bán</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
			  	<li><a href="{{route('client.sell.products')}}">
					<i class="fa fa-circle-o"></i> <span>Đang bán</span>
				  </a>
				 </li> 
				<li><a href="{{route('client.sell.request')}}">
					<i class="fa fa-circle-o"></i> <span>Đơn hỏi hàng</span>
				  </a>
				 </li> 
				 <li><a href="{{route('client.sell.response')}}">
					<i class="fa fa-circle-o"></i> <span>Đơn chào hàng</span>
				  </a>
				 </li> 
			  </ul>
			</li>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-clone"></i> <span>Proposals</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="your_proposals.html"><i class="fa fa-circle-o"></i> Your Proposals</a></li>
				<li><a href="other_proposals.html"><i class="fa fa-circle-o"></i> Other Proposals</a></li>
			  </ul>
			</li>
			<li>
			  <a href="{{route('client.product.create')}}">
				<i class="fa fa-file"></i> <span>Tạo sản phẩm</span>
			  </a>
			</li>
			<li>
			  <a href="offers.html">
				<i class="fa fa-file"></i> <span>Offers</span>
			  </a>
			</li>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-copyright"></i> <span>Companies</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="companylist.html"><i class="fa fa-circle-o"></i> Company List</a></li>
				<li><a href="addcompany.html"><i class="fa fa-circle-o"></i> Add a Company</a></li>
			  </ul>
			</li>
			<li>
			  <a href="messages.html">
				<i class="fa fa-envelope"></i> <span>Messages</span>
				<span class="pull-right-container">
				  <small class="label pull-right bg-green">4</small>
				</span>
			  </a>
			</li>
		 </ul>		
		
		  <ul class="sidebar-menu" data-widget="tree">
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-exchange"></i> <span>Wallet</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="deposits.html"><i class="fa fa-circle-o"></i> Deposits</a></li>
				<li><a href="withdrawals.html"><i class="fa fa-circle-o"></i> Withdrawals</a></li>
			  </ul>
			</li>
			<li>
			  <a href="payment_method.html">
				<i class="fa fa-dot-circle-o"></i> <span>Payment Method</span>
			  </a>
			</li>
			<li>
			  <a href="membership.html">
				<i class="fa fa-credit-card"></i> <span>Membership</span>
			  </a>
			</li>
		 </ul>	
		 
		  <ul class="sidebar-menu" data-widget="tree">
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-external-link-square"></i> <span>Feature a Job</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="featured_job_list.html"><i class="fa fa-circle-o"></i> Jobs Featured List</a></li>
				<li><a href="feature_a_job.html"><i class="fa fa-circle-o"></i> Feature a Job</a></li>
			  </ul>
			</li>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-external-link-square"></i> <span>Feature a Profile</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="featured_profiles.html"><i class="fa fa-circle-o"></i> Profile Featured List</a></li>
				<li><a href="feature_your_profile.html"><i class="fa fa-circle-o"></i> Feature your Profile </a></li>
			  </ul>
			</li>
			<li class="treeview">
			  <a href="#">
				<i class="fa fa-external-link-square"></i> <span>Feature a Company</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="featured_companies.html"><i class="fa fa-circle-o"></i> Companies Featured List</a></li>
				<li><a href="feature_your_company.html"><i class="fa fa-circle-o"></i> Feature your Companies </a></li>
			  </ul>
			</li>
		 </ul>
		
		  <ul class="sidebar-menu" data-widget="tree">
			<li>
			  <a href="editprofile.html">
				<i class="fa fa-user"></i> <span>Edit Profile</span>
			  </a>
			</li>
			<li>
			  <a href="profileimage.html">
				<i class="fa fa-image"></i> <span>Change Profile Image</span>
			  </a>
			</li>
			<li>
			  <a href="password.html">
				<i class="fa fa-lock"></i> <span>Change Password</span>
			  </a>
			</li>
		 </ul>			 
		       		
	    </div>