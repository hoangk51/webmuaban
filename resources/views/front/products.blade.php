@extends('front.layout.app')
@section('content')
<section class="featured-users">	
      <div class="container">
			<div class="section-title" style="padding-top: 20px;">
				<h1>Sản phẩm</h1>
			</div>
		
	  <div class="row"> 
      <div class="col-lg-9" id="table">	
      	 
	  @include('front.products_data')
	  
       </div><!-- col-md-9 -->	
	   
		<div class="col-sm-4 col-md-3">
			
		 <div class="widget">
		    <h3 class="widget_title">Danh mục</h3>
			<ul class="tr-list">
				@foreach($categories as $category)
			 <li><a href="#" class="active"><i class="fa fa-code"></i>{{$category->name}}</a></li>
			@endforeach
			</ul>
		  <!--  <div class="margin-space"></div>
		   <div class="row">
		    <div class="col-sm-6">
		     <h3 class="widget_title_small">Payment Type</h3>
			<ul class="tr-list">
			 <li><a href="" class="active">Any</a></li>
			 <li><a href="">By Hour</a></li>
			 <li><a href="">Fixed Cost</a></li>
			</ul>
			</div>
		    <div class="col-sm-6">
		     <h3 class="widget_title_small">Experience Level</h3>
			<ul class="tr-list">
			 <li><a href="">Entry Level</a></li>
			 <li><a href="">Intermediate</a></li>
			 <li><a href="">Expert</a></li>
			</ul>
			</div>
		   </div>
		   <div class="margin-space"></div>
		   <div class="row">
		    <div class="col-sm-6">
		     <h3 class="widget_title_small">Job Duration</h3>
			<ul class="tr-list">
			 <li><a href="">6+ Months</a></li>
			 <li><a href="">3 - 6 Months</a></li>
			 <li><a href="">1 - 3 Months</a></li>
			 <li><a href="">Below 1 Month</a></li>
			 <li><a href="">Below 1 Week</a></li>
			</ul>
			</div>
		    <div class="col-sm-6">
		     <h3 class="widget_title_small">Hours Per Week</h3>
			<ul class="tr-list">
			 <li><a href="">30 - 39</a></li>
			 <li><a href="">20 - 29</a></li>
			 <li><a href="">10 - 19</a></li>
			 <li><a href="">1 - 9</a></li>
			</ul>
			</div>
		   </div> -->
		   
		 </div><!-- /.widget --> 
		
	    </div>
      </div>		
	   
      </div>	
     </section>

@endsection

@section('js')
<script type="text/javascript">

	  function filter(){
        var form = $('#form');
        doSearch(form.serialize());
    }   

    function doSearch(data){
        //var form = $('#form');
        var url = $('#form').attr('action');
        $.ajax({
            type: "GET",
            url: url,
            data: data ,
            success: function(data)
            {
                if(data.status === 'success'){
                    $("#table").html(data.html);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error(xhr.responseJSON.message);
/*
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: xhr.responseJSON.message
                })*/
            }
        });
    }
</script>

@endsection

