<!DOCTYPE html>
<html lang="en">
  <head>

	    <!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>The Kafe - Ultimate Freelance Marketplace Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Add your business website description here">
        <meta name="keywords" content="Add your, business, website, keywords, here">
        <meta name="author" content="Add your business, website, author here">
		
		<!-- ==============================================
		Favicons
		=============================================== --> 
		<link rel="icon" href="images/logo.jpg">
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
		
	    <!-- ==============================================
		CSS
		=============================================== -->
        <!-- Style-->
        <link type="text/css" href="{{asset('front/css/style.css')}} " rel="stylesheet">
        <link type="text/css" href="{{asset('front/css/login.css')}}" rel="stylesheet">
				
		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<script src="{{asset('front/js/modernizr-custom.js')}}"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->	
		
  </head>

  <body>

     <!-- ==============================================
     Navigation Section
     =============================================== -->  
     <header class="tr-header">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
	    <div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		 </button>
		 <a class="navbar-brand" href="home.html"><img src="images/logo.jpg" alt="Image">The Kafe</a>
		</div><!-- /.navbar-header -->
		<div class="navbar-left">
		 <div class="collapse navbar-collapse" id="navbar-collapse">
		  <ul class="nav navbar-nav">
		   <li><a href="hire.html">GoHire</a></li>
		   <li><a href="work.html">GoWork</a></li>
		   <li><a href="pricing.html">Pricing</a></li>
		   <li><a href="how.html">How it works</a></li>
		  </ul>
		 </div>
		</div><!-- /.navbar-left -->
		<div class="navbar-right">                          
		 <ul class="nav navbar-nav">
		  <li><i class="fa fa-user"></i></li>
		  <li class="active"><a href="login.html">Sign In/ Register </a></li>
		  
		 <li class="dropdown mega-avatar">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
		   <span class="avatar w-32"><img src="images/2.jpg" class="img-resonsive img-circle" width="25" height="25" alt="..."></span>
		   <!-- hidden-xs hides the username on small devices so only the image appears. -->
		   <span>
			Alex Grantte
		   </span>
		  </a>
		  <div class="dropdown-menu w dropdown-menu-scale pull-right">
		   <a class="dropdown-item" href="dashboard.html"><span>Dashboard</span></a> 
		   <a class="dropdown-item" href="profile.html"><span>Profile</span></a> 
		   <a class="dropdown-item" href="editprofile.html"><span>Settings</span></a> 
		   <a class="dropdown-item" href="#">Sign out</a>
		  </div>
		 </li><!-- /navbar-item -->	
		 
		 </ul><!-- /.sign-in -->   
		 <a href="addjob.html" class="kafe-btn kafe-btn-mint-small">Post a Job</a>
		</div><!-- /.nav-right -->
       </div><!-- /.container -->
      </nav><!-- /.navbar -->
     </header><!-- Page Header -->  
 	 
     <!-- ==============================================
	 Header
	 =============================================== -->	 
	 <header class="header-login top-page">
      <div class="container">
	   <div class="content">
	    <div class="row">
	     <h1 class="revealOnScroll" data-animation="fadeInDown"> The Kafe</h1>
        </div><!-- /.row -->
       </div><!-- /.content -->
	  </div><!-- /.container -->
     </header><!-- /header -->
	 
    <section class="banner-login">
	  <div class="container">
	  		  	
	   <div class="row">
	   
	    <div class="main main-signup col-lg-12">
	     <div class="col-lg-6 col-lg-offset-3 text-center">
				
	     	
		  <div class="form-sign">
		    <form method="post" action="{{ url('/dang-nhap') }}">
             {!! csrf_field() !!}
		    <div class="form-head">
			 <h3>Login</h3>
			</div><!-- /.form-head -->
			  <div class="margin-space"></div>
			  <a href="register.html" class="kafe-btn btn-facebook full-width">
			  <i class="fa fa-facebook pull-left"></i> Sign in with Facebook</a>
			  <div class="margin-space"></div>
			  <a href="register.html" class="kafe-btn btn-twitter full-width">
			  <i class="fa fa-twitter pull-left"></i> Sign in with Twitter</a>
			  <div class="margin-space"></div>
			  <a href="register.html" class="kafe-btn btn-google-plus full-width">
			  <i class="fa fa-google-plus pull-left"></i> Sign in with Google Plus</a>
			  <div class="margin-space"></div>
            <div class="form-body">            	
            	
			 <div class="form-row">
			  <div class="form-controls">
			    <input type="email" class="field {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}"  placeholder="Email">
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
			  </div><!-- /.form-controls -->
			 </div><!-- /.form-row -->

			 <div class="form-row">
			  <div class="form-controls">
			    <input type="password" class="field {{ $errors->has('password')?'is-invalid':'' }}" placeholder="Password" name="password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                       <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
			  </div><!-- /.form-controls -->
			 </div><!-- /.form-row -->
			 
			 <div class="form-row">
			  <div class="material-switch pull-left">
			   <input id="someSwitchOptionSuccess" name="remember" type="checkbox">
			   <label for="someSwitchOptionSuccess" class="label-success"></label>
			   <span>Remember Me</span>
			  </div>
			 </div><!-- /.form-row -->
			 
		    </div><!-- /.form-body -->

			<div class="form-foot">
			 <div class="form-actions">					
			  <input value="Login" class="kafe-btn kafe-btn-default full-width" type="submit">

			  <div class="margin-space"></div>
			  <a href="{{ url('/register') }}" class="kafe-btn kafe-btn-danger full-width">Register</a>
			 </div><!-- /.form-actions -->
             <div class="form-head">
			  <a href="{{ url('/password/reset') }}" class="more-link">Forgot Password?</a>
			 </div>
			</div><!-- /.form-foot -->
		   </form>
		   
		  </div><!-- /.form-sign -->
	     </div><!-- /.col-lg-6 -->
        </div>
		
	   </div><!-- /.row -->
	  </div><!-- /.container -->
     </section>
	
	
	 <!-- ==============================================
	 Footer Section
	 =============================================== -->
	 <footer class="footerWhite">

      <!-- COPY RIGHT -->
      <div class="clearfix copyRight">
       <div class="container">
        <div class="row">
         
		 <div class="col-xs-12">
          <div class="copyRightWrapper">
           <div class="row">
		   
            <div class="col-sm-5 col-sm-push-7 col-xs-12">
			 <ul class="list-inline socialLink">
			  <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			  <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			 </ul>
            </div><!-- /col-sm-5 -->
			
		    <div class="col-sm-7 col-sm-pull-5 col-xs-12">
			 <div class="copyRightText">
			  <p>Copyright © 2018. All Rights Reserved</p>
			 </div>
		    </div><!-- /col-sm-7 -->
		  
           </div><!-- /row -->
          </div><!-- /copyRightWrapper -->
         </div><!-- /col-xs-2 -->

        </div><!-- /row -->
       </div><!-- /container -->
      </div><!-- /copyRight -->
	  
    </footer>	
	
     <a id="scrollup">Scroll</a>

	 <!-- ==============================================
	 Scripts
	 =============================================== -->
     <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/kafe.js')}}"></script>

  </body>
</html>