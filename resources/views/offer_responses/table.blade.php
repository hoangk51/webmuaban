<div class="table-responsive-sm">
    <table class="table table-striped" id="offerResponses-table">
        <thead>
            <tr>
                <th>Request Id</th>
        <th>Description</th>
        <th>Price</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($offerResponses as $offerResponse)
            <tr>
                <td>{{ $offerResponse->request_id }}</td>
            <td>{{ $offerResponse->description }}</td>
            <td>{{ $offerResponse->price }}</td>
                <td>
                    {!! Form::open(['route' => ['offerResponses.destroy', $offerResponse->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('offerResponses.show', [$offerResponse->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('offerResponses.edit', [$offerResponse->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>