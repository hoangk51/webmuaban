
<li class="nav-item {{ Request::is('products*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('products.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sản phẩm</span>
    </a>
</li>

<li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Danh mục</span>
    </a>
</li>

<li class="nav-item {{ Request::is('offers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('offers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Đơn hỏi hàng</span>
    </a>
</li>
<li class="nav-item {{ Request::is('offerResponses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('offerResponses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Đơn chào hàng</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orders.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Đơn hàng</span>
    </a>
</li>

<li class="nav-item {{ Request::is('pages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('pages.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Trang tĩnh</span>
    </a>
</li>
