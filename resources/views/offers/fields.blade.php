<!-- Seller Id Field -->
    {!! Form::hidden('seller_id',old('seller_id',request()->input('seller_id'))) !!}
    {!! Form::hidden('product_id',old('product_id',request()->input('product_id')))  !!}

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('offers.index') }}" class="btn btn-secondary">Cancel</a>
</div>
