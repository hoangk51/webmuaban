<div class="table-responsive-sm">
    <table class="table table-striped" id="offers-table">
        <thead>
            <tr>
                <th>Seller Id</th>
        <th>Customer Id</th>
        <th>Status</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Product Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($offers as $offer)
            <tr>
                <td>{{ $offer->seller_id }}</td>
            <td>{{ $offer->customer_id }}</td>
            <td>{{ $offer->status }}</td>
            <td>{{ $offer->price }}</td>
            <td>{{ $offer->quantity }}</td>
            <td>{{ $offer->product_id }}</td>
                <td>
                    {!! Form::open(['route' => ['offers.destroy', $offer->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('offers.show', [$offer->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('offers.edit', [$offer->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>