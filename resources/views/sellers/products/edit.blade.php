@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('sellers.index') !!}">Seller</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Seller</strong>
                          </div>
                          <div class="card-body">
                               {!! Form::open([ 'route' => ['seller.product.update',$productId],'method'=>'POST','id'=>'filterForm']) !!}

                              <div class="form-group col-sm-6">
                                  {!! Form::label('price', 'Price:') !!}
                                  {!! Form::text('price', null, ['class' => 'form-control']) !!}
                              </div>
                              <!-- Submit Field -->
                              <div class="form-group col-sm-12">
                                  {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                  <a href="{{ route('sellers.index') }}" class="btn btn-secondary">Cancel</a>
                              </div>

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection