<div class="table-responsive-sm">
    <table class="table table-striped" id="products-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Rating</th>
        <th>Code</th>
         <th>Price</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->rating }}</td>
            <td>{{ $product->code }}</td>
            <td>{{ $product->pivot->price }}</td>
                <td>
                    <div class='btn-group'>
                      
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>