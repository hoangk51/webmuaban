<div class="table-responsive-sm">
    <table class="table table-striped" id="products-table">
        <thead>
            <tr>
        <th>Name</th>
         <th>Price</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($product->sellers as $seller)
            <tr>
            <td>{{ $seller->name }}</td>
            <td>{{ $seller->pivot->price }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('offers.create', ['product_id'=>$product->id,'seller_id'=>$seller->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>