@extends('layouts.app')

@section('content')

     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('products.index') }}">Product</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('products.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                <span class="badge badge-danger" >Đang có {{count($sellers)}} người bán
                                </span>
                                 <span class="badge badge-success" >
                                Đang có {{count($customers)}} người theo dõi
                                </span>
                                @if(!in_array($userId,$sellers))
                                <a> class="btn btn-block btn-danger" href="{{route('product.sell',$product->id)}}" >Sell this product</a>
                                @else
                                <span class="badge badge-success" >
                                Đang bán sp này
                                 </span>
                                @endif

                                 @if(!in_array($userId,$customers))
                                <a class="btn btn-block btn-danger" href="{{route('product.follow',$product->id)}}" >Follow this product</a>
                                @else
                                <span class="badge badge-success" >
                                Đã follow
                                </span>
                                 @endif


                                 @include('products.prices')
                                 @include('products.show_fields')
                                
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
