<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('products', 'ProductAPIController');

Route::resource('menuses', 'MenusAPIController');

Route::resource('books', 'BookAPIController');

Route::resource('hhhs', 'hhhAPIController');

Route::resource('as', 'aAPIController');

Route::resource('dfs', 'dfAPIController');

Route::resource('sliders', 'SliderAPIController');

Route::resource('brands', 'BrandAPIController');

Route::resource('sellers', 'SellerAPIController');

Route::resource('customers', 'CustomerAPIController');

Route::resource('offers', 'OfferAPIController');

Route::resource('offer_responses', 'OfferResponseAPIController');

Route::resource('orders', 'OrderAPIController');

Route::resource('categories', 'CategoryAPIController');

Route::resource('pages', 'PageAPIController');