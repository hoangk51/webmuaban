<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');





Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function()
{
	Route::resource('offers', 'OfferController');

	Route::resource('offerResponses', 'OfferResponseController');

	Route::resource('orders', 'OrderController');

	Route::resource('categories', 'CategoryController');

	Route::resource('pages', 'PageController');

	Route::resource('products', 'ProductController');


});

Route::group(['namespace' => 'Client', 'as' => 'client.'], function()
{

  	Route::get('dang-ky', 'Auth\RegisterController@showRegistrationForm')->name('register');
  	Route::post('dang-ky', 'Auth\RegisterController@register')->name('register.submit');

	Route::get('dang-nhap', 'Auth\LoginController@showLoginForm')->name('login');
 	Route::post('dang-nhap', 'Auth\LoginController@login')->name('login.post');

 	Route::post('/dang-xuat' , 'Auth\LoginController@logout')->name('logout');

 	Route::get('dashboard', 'DashboardController@index')->name('dashboard');

 	

	Route::get('sell/product/{product}', 'ProductController@sell')->name('product.sell');
	Route::post('follow/product/{product}', 'ProductController@follow')->name('product.follow');

	Route::get('follow/products', 'ProductController@followList')->name('follow.products');

	
	Route::get('response/offer/create/{offer}', 'OfferResponseController@create')->name('response.offer.create');
	Route::post('response/offer/store', 'OfferResponseController@store')->name('response.offer.store');
	Route::get('responses/offer/{offerId}', 'OfferResponseController@index')->name('response.offer.index');


	Route::get('buy/offer/list', 'BuyController@offer')->name('buy.offer.index');
	Route::get('buy/offer/create', 'OfferController@create')->name('offer.create');
	Route::post('buy/offer/store', 'OfferController@store')->name('offer.store');
	
	Route::get('buy/responses/{offerId}', 'BuyController@responses')->name('buy.response.index');
	Route::get('buy/follow/products', 'BuyController@followList')->name('buy.follow.products');

	Route::get('buy/order/list', 'BuyController@listOrder')->name('buy.order.list');
	Route::get('buy/order/create/{response}', 'BuyController@createOrder')->name('buy.order.create');
	Route::post('buy/order/store', 'BuyController@storeOrder')->name('buy.order.store');
	Route::get('user/edit', 'UserController@edit')->name('user.edit');
	Route::post('user/update', 'UserController@update')->name('user.update');
	Route::get('them-ma-moi', 'ProductController@create')->name('product.create');
	Route::post('them-ma-moi', 'ProductController@store')->name('product.store');

	Route::get('sell/products', 'SellController@sellList')->name('sell.products');
	Route::get('sell/response', 'SellController@response')->name('sell.response');
	Route::get('sell/request', 'SellController@request')->name('sell.request');
	Route::get('sell/answer/{offer}', 'SellController@createAnswer')->name('sell.answer.create');
	Route::post('sell/answer/store', 'SellController@storeAnswer')->name('sell.answer.store');

	Route::get('sell/product/price/edit/{product}', 'SellController@editPrice')->name('sell.price.edit');
	Route::post('sell/product/price/update/{product}', 'SellController@updatePrice')->name('sell.price.update');
});

 	Route::get('/', 'Front\HomeController@index')->name('home');
 	Route::get('product/{product}', 'Front\HomeController@detail')->name('product.detail');
 	Route::get('san-pham', 'Front\HomeController@products')->name('product.list');
 	Route::get('tim-kiem', 'Front\HomeController@productSearch')->name('product.search');

 	Route::get('profile/{user}', 'Front\ProfileController@index')->name('profile');





