<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\CreateOfferResponseRequest;
use App\Http\Requests\UpdateOfferResponseRequest;
use App\Repositories\OfferResponseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Offer;
class OfferResponseController extends AppBaseController
{
    /** @var  OfferResponseRepository */
    private $offerResponseRepository;

    public function __construct(OfferResponseRepository $offerResponseRepo)
    {
        $this->offerResponseRepository = $offerResponseRepo;
    }

    /**
     * Display a listing of the OfferResponse.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        echo 123;die;
       $responses =  \DB::table('offer_responses as off')
        ->select('u.name as seller_name','off.price' ,'o.price as customer_price', 'o.quantity' ,'p.name as product_name','off.created_at as responses_time','o.created_at as request_time')
        ->join('offers as o','off.request_id', '=', 'o.id')
        ->join('sellers as s','off.seller_id', '=', 's.id')
        ->join('users as u','u.seller_id', '=', 's.id')
        ->join('products as p','o.product_id', '=', 'p.id')
        ->where('o.customer_id',2)
        ->get();
       // dd($responses);
        return view('front.client.buy.responses')
            ->with('responses', $responses);
    }

    /**
     * Show the form for creating a new OfferResponse.
     *
     * @return Response
     */
    public function create(Request $request,Offer $offer)
    {
        return view('front.client.create_offer_responses',compact('offer'));
    }

    /**
     * Store a newly created OfferResponse in storage.
     *
     * @param CreateOfferResponseRequest $request
     *
     * @return Response
     */
    public function store(CreateOfferResponseRequest $request)
    {
        $input = $request->all();

        $offerResponse = $this->offerResponseRepository->create($input);

        Flash::success('Offer Response saved successfully.');

        return redirect(route('offerResponses.index'));
    }

    /**
     * Display the specified OfferResponse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        return view('offer_responses.show')->with('offerResponse', $offerResponse);
    }

    /**
     * Show the form for editing the specified OfferResponse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        return view('offer_responses.edit')->with('offerResponse', $offerResponse);
    }

    /**
     * Update the specified OfferResponse in storage.
     *
     * @param int $id
     * @param UpdateOfferResponseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOfferResponseRequest $request)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        $offerResponse = $this->offerResponseRepository->update($request->all(), $id);

        Flash::success('Offer Response updated successfully.');

        return redirect(route('offerResponses.index'));
    }

    /**
     * Remove the specified OfferResponse from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        $this->offerResponseRepository->delete($id);

        Flash::success('Offer Response deleted successfully.');

        return redirect(route('offerResponses.index'));
    }
}
