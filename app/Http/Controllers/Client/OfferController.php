<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\CreateOfferRequest;
use App\Http\Requests\UpdateOfferRequest;
use App\Repositories\OfferRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Offer;
use App\Models\Product;

class OfferController extends AppBaseController
{
    /** @var  OfferRepository */
    private $offerRepository;

    public function __construct(OfferRepository $offerRepo)
    {
        $this->offerRepository = $offerRepo;
    }

    /**
     * Display a listing of the Offer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sells = \Auth()->user()->seller->products->pluck('id')->toArray();
        //Offer
        $offers = Offer::whereIn('product_id',$sells)->paginate(10) ;// $this->offerRepository->paginate(10);
      //  dd($offers);

        return view('front.client.offers_request')
            ->with('offers', $offers);
    }

    /**
     * Show the form for creating a new Offer.
     *
     * @return Response
     */
    public function create()
    {
        return view('front.client.create_offer');
    }

    /**
     * Store a newly created Offer in storage.
     *
     * @param CreateOfferRequest $request
     *
     * @return Response
     */
    public function store(CreateOfferRequest $request)
    {
        $request->request->add(['customer_id' => \Auth()->user()->customer_id,'status' => 1]);
        $input = $request->all();
        $request->request->add(['variable' => 'value']); //add request

         //send offer to all realtive seller 
        $sellers= $this->getSellers($input['product_id']);

        $offer = $this->offerRepository->create($input);
        $offer->sellers()->attach($sellers);
        Flash::success('Offer saved successfully.');

        return redirect(route('offers.index'));
    }

     public function getSellers($productId){
         $product = Product::find($productId );
         $sellers= $product->sellers->pluck('id')->toArray();
          return  $sellers;
     }
    /**
     * Display the specified Offer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        return view('offers.show')->with('offer', $offer);
    }

    /**
     * Show the form for editing the specified Offer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        return view('offers.edit')->with('offer', $offer);
    }

    /**
     * Update the specified Offer in storage.
     *
     * @param int $id
     * @param UpdateOfferRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOfferRequest $request)
    {
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        $offer = $this->offerRepository->update($request->all(), $id);

        Flash::success('Offer updated successfully.');

        return redirect(route('offers.index'));
    }

    /**
     * Remove the specified Offer from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        $this->offerRepository->delete($id);

        Flash::success('Offer deleted successfully.');

        return redirect(route('offers.index'));
    }

    public function responses(Request $request , $offerId)
    {

       $responses =  \DB::table('offer_responses as off')
        ->select('u.name as seller_name','off.price' ,'o.price as customer_price', 'o.quantity' ,'p.name as product_name','off.created_at as responses_time','o.created_at as request_time')
        ->join('offers as o','off.request_id', '=', 'o.id')
        ->join('sellers as s','off.seller_id', '=', 's.id')
        ->join('users as u','u.seller_id', '=', 's.id')
        ->join('products as p','o.product_id', '=', 'p.id')
        ->where('o.customer_id',2)
        ->where('o.id',$offerId)
        ->get();
       // dd($responses);
        return view('front.client.buy.responses')
            ->with('responses', $responses);
    }

}
