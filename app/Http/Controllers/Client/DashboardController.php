<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Order;
use DB;
class DashboardController extends Controller
{

    public function index()
    {
    	$user = \Auth()->user();
    	//echo $user->seller->id;die;
    	$totalSell = Order::where('seller_id',$user->seller->id )->count();
    	$totalBuy = Order::where('customer_id',$user->customer->id )->count();
    	$sell = DB::table("orders")->selectRaw('orders.price*orders.quantity as total')->where('seller_id',$user->seller->id )->first();
    	$amountSell= isset($sell->total) ? $sell->total : 0;
    	//dd($amountSell->total);

        return view('front.client.dashboard',compact('totalSell','totalBuy','amountSell'));
    }
    public function __construct()
    {
    }
}
