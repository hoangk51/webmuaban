<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\CreateOfferRequest;
use App\Http\Requests\UpdateOfferRequest;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\CreateOfferResponseRequest;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Product;
use App\Models\Offer;
use App\Models\OfferResponse;
use App\Repositories\OfferResponseRepository;

class SellController extends AppBaseController
{
    /** @var  OfferRepository */
    protected $seller; 
    private $offerResponseRepository;


    public function __construct(OfferResponseRepository $offerResponseRepo)
    {
        $this->offerResponseRepository = $offerResponseRepo;
    }

    
    
    public function sellList(Request $request)
    {
        $products = \Auth()->user()->seller->products;
        return view('front.client.sell.products',compact('products'));
    }

     public function response(Request $request)
    {
        $sellerId = \Auth()->user()->seller_id;
        $responses =  \DB::table('offer_responses as off')
        ->select('u.name as buyer_name','off.id','off.price' ,'off.quantity as sell_quantity','o.price as customer_price', 'o.quantity' ,'p.name as product_name','off.created_at as responses_time','o.created_at as request_time')
        ->join('offers as o','off.request_id', '=', 'o.id')
        ->join('customers as c','o.customer_id', '=', 'c.id')
        ->join('users as u','u.seller_id', '=', 'c.id')
        ->join('products as p','o.product_id', '=', 'p.id')
        ->where('off.seller_id',$sellerId )
        ->get();
       // dd($responses);
        return view('front.client.sell.response')
            ->with('responses', $responses);
    }
     public function request(Request $request){
        $this->seller =  \Auth()->user()->seller;
        $offers = $this->seller->offers;
         return view('front.client.sell.offers')
            ->with('offers', $offers);
     }

     public function createAnswer(Request $request,Offer $offer){
         return view('front.client.sell.answer_form')->with('offer', $offer);
     }
     public function storeAnswer(CreateOfferResponseRequest $request){
        $request->request->add(['seller_id' => \Auth()->user()->seller_id]);
        $input = $request->all();
       // OfferResponse::insert($input);
        $offerResponse = $this->offerResponseRepository->create($input);
           return response()->json([
                'message'=> 'Tạo đơn chào hàng thành công',
                'status'=>'success'
            ]);
     }

     public function editPrice(Request $request,Product $product)
    {
        return view('front.client.sell.price_form',compact('product'));
    }
     public function updatePrice(Request $request,Product $product)
    {
       //echo $productId ;die;
        /*dd( $product);
       echo $request->input('price');die;*/
        $seller = \Auth()->user()->seller;
        $price= $request->input('price');
       $seller->products()->sync([$product->id=>['price' => $price]]);
      /* \App\PriceHistory::create(['seller_id'=>$seller->id,
                                'price'=>$price,
                                ]);*/
        return response()->json([
            'message'=> 'Cập nhật giá thành công',
            'status'=>'success'
        ]);

    }
}
