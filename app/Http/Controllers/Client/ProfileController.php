<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\User;

class ProfileController extends AppBaseController
{
  
    public function __construct()
    {
    }

    
    public function index(Request $request , User $user )
    {
        return view('front.client.profile',compact('user'));
    }
}
