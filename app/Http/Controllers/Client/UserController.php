<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\User;
use App\Models\City;

class UserController extends AppBaseController
{
  
    public function __construct()
    {
    }

    
    public function edit(Request $request )
    {
    	$user = \Auth()->user();
        $cities = City::pluck('name','id')->toArray();
        return view('front.client.user',compact('user','cities'));
    }

    public function update(Request $request )
    {
        $input = $request->all();
        $user = \Auth()->user();
         $user->update($input);
       /* $order = $this->orderRepository->create($input);*/
       if ($request->hasFile('thumbnail'))
            $user->saveImage($request->file('thumbnail'));


        Flash::success('Profile saved successfully.');

        return redirect(route('client.user.edit'));
    }
}
