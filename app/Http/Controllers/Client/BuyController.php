<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\CreateOfferRequest;
use App\Http\Requests\UpdateOfferRequest;
use App\Http\Requests\CreateOrderRequest;

use App\Repositories\OfferRepository;
use App\Repositories\OrderRepository;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Offer;
use App\Models\OfferResponse;
use App\Models\Order;
//use App\Models\City;

class BuyController extends AppBaseController
{
    /** @var  OfferRepository */
    private $offerRepository;
    private $orderRepository;



    public function __construct(OfferRepository $offerRepo,OrderRepository $orderRepo)
    {
        $this->offerRepository = $offerRepo;
        $this->orderRepository = $orderRepo;
    }

    
    public function responses(Request $request , $offerId)
    {

       $responses =  \DB::table('offer_responses as off')
        ->select('u.name as seller_name','off.id','off.price' ,'off.quantity as sell_quantity','o.price as customer_price', 'o.quantity' ,'p.name as product_name','off.created_at as responses_time','o.created_at as request_time')
        ->join('offers as o','off.request_id', '=', 'o.id')
        ->join('sellers as s','off.seller_id', '=', 's.id')
        ->join('users as u','u.seller_id', '=', 's.id')
        ->join('products as p','o.product_id', '=', 'p.id')
       // ->where('o.customer_id',2)
        ->where('o.id',$offerId)
        ->get();
       // dd($responses);
        return view('front.client.buy.responses')
            ->with('responses', $responses);
    }

    public function listOrder(Request $request ){
        $orders =  Order::with(['seller','product'])->where('customer_id', \Auth()->user()->customer_id )->paginate(10);

        return view('front.client.buy.order.list')
            ->with('orders', $orders);
    }


    public function createOrder(Request $request ,OfferResponse $response){
        //$cities = City::pluck('name','id')->toArray();,compact('cities')
        return view('front.client.buy.order.create')
            ->with('response', $response);
    }

    public function storeOrder(CreateOrderRequest $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->create($input);

        Flash::success('Order saved successfully.');

        return redirect(route('orders.index'));
    }

      public function offer(Request $request)
    {
        //$sells = \Auth()->user()->seller->products->pluck('id')->toArray();
        //Offer
        $offers = Offer::where('customer_id', \Auth()->user()->customer_id)->paginate(10) ;// $this->offerRepository->paginate(10);
      //  dd($offers);

        return view('front.client.offers_request')
            ->with('offers', $offers);
    }

     public function followList(Request $request)
    {
        $customer = \Auth()->user()->customer;
        $products = $customer->products;
        return view('front.client.follows',compact('products'));

    }

    
}
