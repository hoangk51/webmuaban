<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Product;
use App\Models\Category;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $products = $this->productRepository->paginate(10);


        return view('products.index')
            ->with('products', $products);
    }

     public function create()
    {
        $categories = Category::pluck('name','id')->toArray();
        return view('front.client.products.create',compact('categories'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $request->request->add(['user_id' => \Auth()->user()->id,'status' => 0]);
        $input = $request->all();
        $product = $this->productRepository->create($input);

        Flash::success('Product saved successfully.');

        return redirect(route('home'));
    }

    public function sell(Product $product)
    {
        $user = \Auth()->user();
        $product->sellers()->sync($user->id);
        return view('front.product.price_create');
    }

    public function follow(Product $product)
    {
        
        $user = \Auth()->user();
        $product->customers()->attach($user->id);

         return response()->json([
            'status' => 'success',
           'message' => 'theo dõi thành công',
        ]);
    }
    public function followList(Product $product)
    {
        
        $customer = \Auth()->user()->customer;
        $products = $customer->products;
        return view('front.client.follows',compact('products'));

    }

    public function sellList(Product $product)
    {
        
        $seller = \Auth()->user()->seller;
        $products = $seller->products;
        return view('front.client.sells',compact('products'));

    }
}
