<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSellerAPIRequest;
use App\Http\Requests\API\UpdateSellerAPIRequest;
use App\Models\Seller;
use App\Repositories\SellerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SellerController
 * @package App\Http\Controllers\API
 */

class SellerAPIController extends AppBaseController
{
    /** @var  SellerRepository */
    private $sellerRepository;

    public function __construct(SellerRepository $sellerRepo)
    {
        $this->sellerRepository = $sellerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/sellers",
     *      summary="Get a listing of the Sellers.",
     *      tags={"Seller"},
     *      description="Get all Sellers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Seller")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $sellers = $this->sellerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($sellers->toArray(), 'Sellers retrieved successfully');
    }

    /**
     * @param CreateSellerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/sellers",
     *      summary="Store a newly created Seller in storage",
     *      tags={"Seller"},
     *      description="Store Seller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Seller that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Seller")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Seller"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSellerAPIRequest $request)
    {
        $input = $request->all();

        $seller = $this->sellerRepository->create($input);

        return $this->sendResponse($seller->toArray(), 'Seller saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/sellers/{id}",
     *      summary="Display the specified Seller",
     *      tags={"Seller"},
     *      description="Get Seller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Seller",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Seller"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Seller $seller */
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            return $this->sendError('Seller not found');
        }

        return $this->sendResponse($seller->toArray(), 'Seller retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSellerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/sellers/{id}",
     *      summary="Update the specified Seller in storage",
     *      tags={"Seller"},
     *      description="Update Seller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Seller",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Seller that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Seller")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Seller"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSellerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Seller $seller */
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            return $this->sendError('Seller not found');
        }

        $seller = $this->sellerRepository->update($input, $id);

        return $this->sendResponse($seller->toArray(), 'Seller updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/sellers/{id}",
     *      summary="Remove the specified Seller from storage",
     *      tags={"Seller"},
     *      description="Delete Seller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Seller",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Seller $seller */
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            return $this->sendError('Seller not found');
        }

        $seller->delete();

        return $this->sendSuccess('Seller deleted successfully');
    }
}
