<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOfferResponseAPIRequest;
use App\Http\Requests\API\UpdateOfferResponseAPIRequest;
use App\Models\OfferResponse;
use App\Repositories\OfferResponseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OfferResponseController
 * @package App\Http\Controllers\API
 */

class OfferResponseAPIController extends AppBaseController
{
    /** @var  OfferResponseRepository */
    private $offerResponseRepository;

    public function __construct(OfferResponseRepository $offerResponseRepo)
    {
        $this->offerResponseRepository = $offerResponseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/offerResponses",
     *      summary="Get a listing of the OfferResponses.",
     *      tags={"OfferResponse"},
     *      description="Get all OfferResponses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/OfferResponse")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $offerResponses = $this->offerResponseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($offerResponses->toArray(), 'Offer Responses retrieved successfully');
    }

    /**
     * @param CreateOfferResponseAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/offerResponses",
     *      summary="Store a newly created OfferResponse in storage",
     *      tags={"OfferResponse"},
     *      description="Store OfferResponse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OfferResponse that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OfferResponse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OfferResponse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOfferResponseAPIRequest $request)
    {
        $input = $request->all();

        $offerResponse = $this->offerResponseRepository->create($input);

        return $this->sendResponse($offerResponse->toArray(), 'Offer Response saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/offerResponses/{id}",
     *      summary="Display the specified OfferResponse",
     *      tags={"OfferResponse"},
     *      description="Get OfferResponse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OfferResponse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OfferResponse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var OfferResponse $offerResponse */
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            return $this->sendError('Offer Response not found');
        }

        return $this->sendResponse($offerResponse->toArray(), 'Offer Response retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOfferResponseAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/offerResponses/{id}",
     *      summary="Update the specified OfferResponse in storage",
     *      tags={"OfferResponse"},
     *      description="Update OfferResponse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OfferResponse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OfferResponse that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OfferResponse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OfferResponse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOfferResponseAPIRequest $request)
    {
        $input = $request->all();

        /** @var OfferResponse $offerResponse */
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            return $this->sendError('Offer Response not found');
        }

        $offerResponse = $this->offerResponseRepository->update($input, $id);

        return $this->sendResponse($offerResponse->toArray(), 'OfferResponse updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/offerResponses/{id}",
     *      summary="Remove the specified OfferResponse from storage",
     *      tags={"OfferResponse"},
     *      description="Delete OfferResponse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OfferResponse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var OfferResponse $offerResponse */
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            return $this->sendError('Offer Response not found');
        }

        $offerResponse->delete();

        return $this->sendSuccess('Offer Response deleted successfully');
    }
}
