<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatedfAPIRequest;
use App\Http\Requests\API\UpdatedfAPIRequest;
use App\Models\df;
use App\Repositories\dfRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class dfController
 * @package App\Http\Controllers\API
 */

class dfAPIController extends AppBaseController
{
    /** @var  dfRepository */
    private $dfRepository;

    public function __construct(dfRepository $dfRepo)
    {
        $this->dfRepository = $dfRepo;
    }

    /**
     * Display a listing of the df.
     * GET|HEAD /dfs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $dfs = $this->dfRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dfs->toArray(), 'Dfs retrieved successfully');
    }

    /**
     * Store a newly created df in storage.
     * POST /dfs
     *
     * @param CreatedfAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatedfAPIRequest $request)
    {
        $input = $request->all();

        $df = $this->dfRepository->create($input);

        return $this->sendResponse($df->toArray(), 'Df saved successfully');
    }

    /**
     * Display the specified df.
     * GET|HEAD /dfs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var df $df */
        $df = $this->dfRepository->find($id);

        if (empty($df)) {
            return $this->sendError('Df not found');
        }

        return $this->sendResponse($df->toArray(), 'Df retrieved successfully');
    }

    /**
     * Update the specified df in storage.
     * PUT/PATCH /dfs/{id}
     *
     * @param int $id
     * @param UpdatedfAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedfAPIRequest $request)
    {
        $input = $request->all();

        /** @var df $df */
        $df = $this->dfRepository->find($id);

        if (empty($df)) {
            return $this->sendError('Df not found');
        }

        $df = $this->dfRepository->update($input, $id);

        return $this->sendResponse($df->toArray(), 'df updated successfully');
    }

    /**
     * Remove the specified df from storage.
     * DELETE /dfs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var df $df */
        $df = $this->dfRepository->find($id);

        if (empty($df)) {
            return $this->sendError('Df not found');
        }

        $df->delete();

        return $this->sendSuccess('Df deleted successfully');
    }
}
