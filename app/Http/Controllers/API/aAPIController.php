<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateaAPIRequest;
use App\Http\Requests\API\UpdateaAPIRequest;
use App\Models\a;
use App\Repositories\aRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class aController
 * @package App\Http\Controllers\API
 */

class aAPIController extends AppBaseController
{
    /** @var  aRepository */
    private $aRepository;

    public function __construct(aRepository $aRepo)
    {
        $this->aRepository = $aRepo;
    }

    /**
     * Display a listing of the a.
     * GET|HEAD /as
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $as = $this->aRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($as->toArray(), 'As retrieved successfully');
    }

    /**
     * Store a newly created a in storage.
     * POST /as
     *
     * @param CreateaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateaAPIRequest $request)
    {
        $input = $request->all();

        $a = $this->aRepository->create($input);

        return $this->sendResponse($a->toArray(), 'A saved successfully');
    }

    /**
     * Display the specified a.
     * GET|HEAD /as/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var a $a */
        $a = $this->aRepository->find($id);

        if (empty($a)) {
            return $this->sendError('A not found');
        }

        return $this->sendResponse($a->toArray(), 'A retrieved successfully');
    }

    /**
     * Update the specified a in storage.
     * PUT/PATCH /as/{id}
     *
     * @param int $id
     * @param UpdateaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateaAPIRequest $request)
    {
        $input = $request->all();

        /** @var a $a */
        $a = $this->aRepository->find($id);

        if (empty($a)) {
            return $this->sendError('A not found');
        }

        $a = $this->aRepository->update($input, $id);

        return $this->sendResponse($a->toArray(), 'a updated successfully');
    }

    /**
     * Remove the specified a from storage.
     * DELETE /as/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var a $a */
        $a = $this->aRepository->find($id);

        if (empty($a)) {
            return $this->sendError('A not found');
        }

        $a->delete();

        return $this->sendSuccess('A deleted successfully');
    }
}
