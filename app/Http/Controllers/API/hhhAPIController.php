<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatehhhAPIRequest;
use App\Http\Requests\API\UpdatehhhAPIRequest;
use App\Models\hhh;
use App\Repositories\hhhRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class hhhController
 * @package App\Http\Controllers\API
 */

class hhhAPIController extends AppBaseController
{
    /** @var  hhhRepository */
    private $hhhRepository;

    public function __construct(hhhRepository $hhhRepo)
    {
        $this->hhhRepository = $hhhRepo;
    }

    /**
     * Display a listing of the hhh.
     * GET|HEAD /hhhs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $hhhs = $this->hhhRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($hhhs->toArray(), 'Hhhs retrieved successfully');
    }

    /**
     * Store a newly created hhh in storage.
     * POST /hhhs
     *
     * @param CreatehhhAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatehhhAPIRequest $request)
    {
        $input = $request->all();

        $hhh = $this->hhhRepository->create($input);

        return $this->sendResponse($hhh->toArray(), 'Hhh saved successfully');
    }

    /**
     * Display the specified hhh.
     * GET|HEAD /hhhs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var hhh $hhh */
        $hhh = $this->hhhRepository->find($id);

        if (empty($hhh)) {
            return $this->sendError('Hhh not found');
        }

        return $this->sendResponse($hhh->toArray(), 'Hhh retrieved successfully');
    }

    /**
     * Update the specified hhh in storage.
     * PUT/PATCH /hhhs/{id}
     *
     * @param int $id
     * @param UpdatehhhAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatehhhAPIRequest $request)
    {
        $input = $request->all();

        /** @var hhh $hhh */
        $hhh = $this->hhhRepository->find($id);

        if (empty($hhh)) {
            return $this->sendError('Hhh not found');
        }

        $hhh = $this->hhhRepository->update($input, $id);

        return $this->sendResponse($hhh->toArray(), 'hhh updated successfully');
    }

    /**
     * Remove the specified hhh from storage.
     * DELETE /hhhs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var hhh $hhh */
        $hhh = $this->hhhRepository->find($id);

        if (empty($hhh)) {
            return $this->sendError('Hhh not found');
        }

        $hhh->delete();

        return $this->sendSuccess('Hhh deleted successfully');
    }
}
