<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Order;
use App\Models\Seller;
use View;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.home')
                ->with('totalProduct',Product::where('status',1)->count())
                ->with('totalOrder',Order::count())
                ->with('totalSeller',Seller::count())
                ;
    }
     public function detail(Product $product)
    {   
       // dd($product->customers);
        $sellerArr = $product->sellers->pluck('id')->toArray();
        $customerArr = $product->customers->pluck('id')->toArray();
       // dd($customerArr);
        return view('front.product_detail',compact('product','sellerArr','customerArr'));
    }
     public function products(Request $request)
    {   
        $products = Product::where('status',1)->paginate(10);
        $categories = Category::all();
        return view('front.products',compact('products','categories'));
    }
    
      public function productSearch(Request $request)
    {   
        $input = $request->all();
        $products = Product::where('status',1);
         if(isset($input['code'])){
            $products = $products->where('code', 'LIKE', "%".$input['code']."%");
        }
        if(isset($input['name'])){
            $products = $products->where('name', 'LIKE', "%".$input['name']."%");
        }
         $products = $products ->paginate(1);
        return response()->json([
                'html'=>  View::make('front.products_data',compact('products'))->render(),
                'status'=>'success'
            ]);
    }

}
