<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSellerRequest;
use App\Http\Requests\UpdateSellerRequest;
use App\Repositories\SellerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Product;
class SellerController extends AppBaseController
{
    /** @var  SellerRepository */
    private $sellerRepository;

    public function __construct(SellerRepository $sellerRepo)
    {
        $this->sellerRepository = $sellerRepo;
    }

    /**
     * Display a listing of the Seller.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sellers = $this->sellerRepository->paginate(110);

        return view('sellers.index')
            ->with('sellers', $sellers);
    }

    public function products(Request $request)
    {
        $user = \Auth()->user();
        $seller = $user->seller;
        $products = $seller->products;
        /*foreach ($products as $key => $value) {
           dd($value->pivot);
        }*/
        
        return view('sellers.products.index')
            ->with('products', $products);
    }
    public function editPrice(Request $request,$productId)
    {
       
        return view('sellers.products.edit')
            ->with('productId', $productId);
    }
     public function updatePrice(Request $request, $product)
    {
       //echo $productId ;die;
        /*dd( $product);
       echo $request->input('price');die;*/
        $seller = \Auth()->user()->seller;
        $price= $request->input('price');
       $seller->products()->sync([$product=>['price' => $price]]);
       \App\PriceHistory::create(['seller_id'=>$seller->id,
                                'price'=>$price,
                                ]);
        return redirect(route('sellers.products.index'));

    }
    /**
     * Show the form for creating a new Seller.
     *
     * @return Response
     */
    public function create()
    {
        return view('sellers.create');
    }

    /**
     * Store a newly created Seller in storage.
     *
     * @param CreateSellerRequest $request
     *
     * @return Response
     */
    public function store(CreateSellerRequest $request)
    {
        $input = $request->all();

        $seller = $this->sellerRepository->create($input);

        Flash::success('Seller saved successfully.');

        return redirect(route('sellers.index'));
    }

    /**
     * Display the specified Seller.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        return view('sellers.show')->with('seller', $seller);
    }

    /**
     * Show the form for editing the specified Seller.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        return view('sellers.edit')->with('seller', $seller);
    }

    /**
     * Update the specified Seller in storage.
     *
     * @param int $id
     * @param UpdateSellerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellerRequest $request)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        $seller = $this->sellerRepository->update($request->all(), $id);

        Flash::success('Seller updated successfully.');

        return redirect(route('sellers.index'));
    }

    /**
     * Remove the specified Seller from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        $this->sellerRepository->delete($id);

        Flash::success('Seller deleted successfully.');

        return redirect(route('sellers.index'));
    }
}
