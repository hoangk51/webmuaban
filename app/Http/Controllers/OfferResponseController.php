<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOfferResponseRequest;
use App\Http\Requests\UpdateOfferResponseRequest;
use App\Repositories\OfferResponseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OfferResponseController extends AppBaseController
{
    /** @var  OfferResponseRepository */
    private $offerResponseRepository;

    public function __construct(OfferResponseRepository $offerResponseRepo)
    {
        $this->offerResponseRepository = $offerResponseRepo;
    }

    /**
     * Display a listing of the OfferResponse.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $offerResponses = $this->offerResponseRepository->paginate(10);

        return view('offer_responses.index')
            ->with('offerResponses', $offerResponses);
    }

    /**
     * Show the form for creating a new OfferResponse.
     *
     * @return Response
     */
    public function create()
    {
        return view('offer_responses.create');
    }

    /**
     * Store a newly created OfferResponse in storage.
     *
     * @param CreateOfferResponseRequest $request
     *
     * @return Response
     */
    public function store(CreateOfferResponseRequest $request)
    {
        $input = $request->all();

        $offerResponse = $this->offerResponseRepository->create($input);

        Flash::success('Offer Response saved successfully.');

        return redirect(route('offerResponses.index'));
    }

    /**
     * Display the specified OfferResponse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        return view('offer_responses.show')->with('offerResponse', $offerResponse);
    }

    /**
     * Show the form for editing the specified OfferResponse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        return view('offer_responses.edit')->with('offerResponse', $offerResponse);
    }

    /**
     * Update the specified OfferResponse in storage.
     *
     * @param int $id
     * @param UpdateOfferResponseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOfferResponseRequest $request)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        $offerResponse = $this->offerResponseRepository->update($request->all(), $id);

        Flash::success('Offer Response updated successfully.');

        return redirect(route('offerResponses.index'));
    }

    /**
     * Remove the specified OfferResponse from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $offerResponse = $this->offerResponseRepository->find($id);

        if (empty($offerResponse)) {
            Flash::error('Offer Response not found');

            return redirect(route('offerResponses.index'));
        }

        $this->offerResponseRepository->delete($id);

        Flash::success('Offer Response deleted successfully.');

        return redirect(route('offerResponses.index'));
    }
}
