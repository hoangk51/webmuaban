<?php

namespace App\Repositories;

use App\Models\OfferResponse;
use App\Repositories\BaseRepository;

/**
 * Class OfferResponseRepository
 * @package App\Repositories
 * @version March 27, 2020, 9:11 am UTC
*/

class OfferResponseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'request_id',
        'description',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OfferResponse::class;
    }
}
