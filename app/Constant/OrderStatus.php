<?php
namespace App\Constant;

class OrderStatus
{
    const PENDING = 1;
    const RECEIVE_PARTAL = 2;
    const FINISH = 3;

    public static $types = [
        self::PENDING => 'hoãn',
        self::RECEIVE_PARTAL => 'thanh toán một phần',
        self::FINISH => 'hoàn thành',

    ];
}