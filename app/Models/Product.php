<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use App\Common\Imageable;

/**
 * @SWG\Definition(
 *      definition="Product",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rating",
 *          description="rating",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      )
 * )
 */
class Product extends Model implements HasMedia
{
    use SoftDeletes,HasMediaTrait,Imageable;

    public $table = 'products';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        'name',
        'description',
        'rating',
        'code',
        'category_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'rating' => 'integer',
        'code' => 'string',
        'status' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

     /*public function categories()
    {
        return $this->belongsToMany(Category::class)->withTimestamps();
    }*/

    public function sellers()
    {
        return $this->belongsToMany(Seller::class)->withPivot('price');//->withTimestamps()
    }
    public function customers()
    {
        return $this->belongsToMany(Customer::class);//->withTimestamps()
    }
    public function category()
    {
        return $this->belongsTo(Category::class);//->withTimestamps()
    }
     public function getLargeThumb()
    {
        //return $this->thumbnail('images', 'large');
        if(null !== $this->getMedia('thumbnail')->last()){ 
            return $this->getMedia('thumbnail')->last()->getUrl('large');
        }else{
           return "https://via.placeholder.com/890x500";
        } 
    }
    
   public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('small')
             ->nonQueued()
             ->crop('crop-center', 45 , 45 );

        $this->addMediaConversion('medium')
            ->nonQueued()
             ->crop('crop-center', 480 , 270 );
/*
            ->width(320)
            ->height(180);*/
            
        $this->addMediaConversion('large')
             ->nonQueued()
             ->crop('crop-center', 890 , 500 );
           /* ->width(890)
            ->height(500);    */    
    }
}
