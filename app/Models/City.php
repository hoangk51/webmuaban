<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class a
 * @package App\Models
 * @version March 16, 2020, 7:04 am UTC
 *
 * @property integer s
 */
class City extends Model
{

    public $table = 'cities';
    



    public $fillable = [
        'name',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'type' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
