<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Offer",
 *      required={""},
 *      @SWG\Property(
 *          property="seller_id",
 *          description="seller_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Offer extends Model
{
    use SoftDeletes;

    public $table = 'offers';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'status',
        'price',
        'quantity',
        'product_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'status' => 'boolean',
        'price' => 'string',
        'quantity' => 'integer',
        'product_id' => 'integer',
        'description'  => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
     public function product()
    {
        return $this->belongsTo(Product::class);
    }

     public function sellers()
    {
        return $this->belongsToMany(Seller::class);
    }
}
