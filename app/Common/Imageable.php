<?php

namespace App\Common;


/**
 * Attach this Trait to a User (or other model) for easier read/writes on Replies
 *
 * @author Munna Khan
 */
trait Imageable {
	public function saveImage($image){
		 $this->addMedia($image)->toMediaCollection('thumbnail', env('FILESYSTEM_DRIVER', 'local'));
	}
}