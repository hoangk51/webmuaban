<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use App\Common\Imageable;

class User extends Authenticatable implements MustVerifyEmail,HasMedia
{
    use Notifiable,HasMediaTrait,Imageable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','description','mst','address','city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class);
    }
     public function seller()
    {
        return $this->belongsTo(\App\Models\Seller::class);
    }

     public function city()
    {
        return $this->belongsTo(\App\Models\City::class);
    }

     public function getLargeThumb()
    {
        //return $this->thumbnail('images', 'large');
        if(null !== $this->getMedia('thumbnail')->last()){ 
            return $this->getMedia('thumbnail')->last()->getUrl('large');
        }else{
           return "https://via.placeholder.com/890x500";
        } 
    }
     public function getSmallThumb()
    {
        //return $this->thumbnail('images', 'large');
        if(null !== $this->getMedia('thumbnail')->last()){ 
            return $this->getMedia('thumbnail')->last()->getUrl('small');
        }else{
           return "https://via.placeholder.com/45x45";
        } 
    }


   public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('small')
             ->nonQueued()
             ->crop('crop-center', 60 , 60 );

        $this->addMediaConversion('medium')
            ->nonQueued()
             ->crop('crop-center', 480 , 270 );
/*
            ->width(320)
            ->height(180);*/
            
        $this->addMediaConversion('large')
             ->nonQueued()
             ->crop('crop-center', 890 , 500 );
           /* ->width(890)
            ->height(500);    */    
    }
}
