<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'seller_id' => $faker->randomDigitNotNull,
        'customer_id' => $faker->randomDigitNotNull,
        'quantity' => $faker->word,
        'price' => $faker->word,
        'name' => $faker->word,
        'email' => $faker->word,
        'phone' => $faker->word
    ];
});
