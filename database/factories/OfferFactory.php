<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Offer;
use Faker\Generator as Faker;

$factory->define(Offer::class, function (Faker $faker) {

    return [
        'seller_id' => $faker->randomDigitNotNull,
        'customer_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'status' => $faker->word,
        'price' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'product_id' => $faker->randomDigitNotNull
    ];
});
