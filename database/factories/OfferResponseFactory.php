<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OfferResponse;
use Faker\Generator as Faker;

$factory->define(OfferResponse::class, function (Faker $faker) {

    return [
        'request_id' => $faker->randomDigitNotNull,
        'description' => $faker->word,
        'price' => $faker->randomDigitNotNull
    ];
});
